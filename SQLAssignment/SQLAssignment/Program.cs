﻿using SQLAssignment.Models;
using SQLAssignment.Repositories;
using System;
using System.Collections.Generic;

namespace SQLAssignment
{
    class Program
    {
        static void Main(string[] args)
        {
            ICustomerRepository customer = new CustomerRepository();
            ICountryRepository country = new CountryRepository();
            ISpenderRepository spender = new SpenderRepository();
            IGenreRepository genre = new GenreRepository();
            //1- SelectAll(customer);
            //2- SelectOne(customer);
            //3- SelectByName(customer);
            //4- SelectAllWithLimit(customer);
            //5- Insert(customer);
            //6- Update(customer);
            //7- SelectAllCountrys(country);
            //8- SelectAllSpenders(spender);
            //9- SelectTopGenre(genre);
        }

        static void SelectAll(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetAll());
        }

        static void SelectOne(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetById(1));
        }

        static void SelectByName(ICustomerRepository repository)
        {
            PrintCustomers(repository.GetCustomerByName("Cr"));
        }

        static void SelectAllWithLimit(ICustomerRepository repository)
        {
            PrintCustomersWithLimit(repository.GetCustomersByLimit(10, 5));
        }

        static void SelectAllCountrys(ICountryRepository repository)
        {
            PrintCountrys(repository.GetAll());
        }

        static void SelectAllSpenders(ISpenderRepository repository)
        {
            PrintSpenders(repository.GetAll());
        }

        static void SelectTopGenre(IGenreRepository repository)
        {
            PrintGenres(repository.GetAll(12));
        }

        static void Insert(ICustomerRepository repository)
        {
            Customer customerObj = new Customer()
            {
                FirstName = "Christian",
                LastName = "Andersz",
                Country = "Sweden",
                PostalCode = "146 34",
                Phone = "+46 73 563 50 37",
                Email = "christian.andersz@se.experis.com"
            };
            if (repository.Add(customerObj))
            {
                Console.WriteLine("Insert worked!");
            }
            else
            {
                Console.WriteLine("Didnt work!");
            }

        }

        static void Update(ICustomerRepository repository)
        {

            Customer customerObj = new Customer()
            {
                CustomerId = 1,
                FirstName = "Christian 'Crille' ",
                LastName = "Andersz",
                Country = "Sweden",
                PostalCode = "146 34",
                Phone = "+46 73 563 50 37",
                Email = "christian.andersz@se.experis.com"
            };
            if (repository.Edit(customerObj))
            {
                Console.WriteLine("Update worked!");
            }
            else
            {
                Console.WriteLine("Update failed!");
            }

        }

        static void PrintCustomers(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomers(customer);
            }
        }

        private static void PrintCustomers(Customer customer)
        {
            Console.WriteLine($"--- {customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Email} {customer.Country} {customer.PostalCode} {customer.Phone} ---");
        }

        static void PrintCustomersWithLimit(IEnumerable<Customer> customers)
        {
            foreach (Customer customer in customers)
            {
                PrintCustomersWithLimit(customer);
            }
        }

        private static void PrintCustomersWithLimit(Customer customer)
        {
            Console.WriteLine($"--- {customer.CustomerId} {customer.FirstName} {customer.LastName} {customer.Email} {customer.Country} {customer.PostalCode} {customer.Phone} ---");
        }

        static void PrintCountrys(IEnumerable<CustomerCountry> customerCountry)
        {
            foreach (CustomerCountry country in customerCountry)
            {
                PrintCountry(country);
            }
        }

        private static void PrintCountry(CustomerCountry country)
        {
            Console.WriteLine($"--- Country: {country.Country} Customers: {country.CustomerId}---");
        }

        static void PrintSpenders(IEnumerable<CustomerSpender> customerSpender)
        {
            foreach (CustomerSpender spender in customerSpender)
            {
                PrintSpender(spender);
            }
        }

        private static void PrintSpender(CustomerSpender customerSpender)
        {
            Console.WriteLine($"--- CustomerID: {customerSpender.CustomerId} Total: {customerSpender.Total}---");
        }

        static void PrintGenres(IEnumerable<CustomerGenre> customerGenre)
        {
            foreach (CustomerGenre genre in customerGenre)
            {
                PrintGenre(genre);
            }
        }

        private static void PrintGenre(CustomerGenre customerGenre)
        {

            Console.WriteLine($"--- First name: {customerGenre.FirstName} Top genre: {customerGenre.Genre}---");

        }

    }
}
