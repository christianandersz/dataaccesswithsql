﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssignment.Models
{
    public class CustomerSpender
    {
        public decimal Total { get; set; }

        public int CustomerId { get; set; }
    }
}
