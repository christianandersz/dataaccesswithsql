﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssignment.Models
{
    public class CustomerGenre
    {
        public string FirstName { get; set; }
        public string Genre { get; set; }
        public int CustomerId { get; set; }
    }
}
