﻿using SQLAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssignment.Repositories
{
    public interface ICountryRepository
    {
        /// <summary>
        /// Get quantity of customers in each country. Ordered by most customers.  
        /// </summary>
        /// <returns>Returns quantity of customers in each country.</returns>
        public IEnumerable<CustomerCountry> GetAll();
    }
}
