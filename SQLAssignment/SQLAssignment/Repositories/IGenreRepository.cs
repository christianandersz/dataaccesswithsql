﻿using SQLAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssignment.Repositories
{
    public interface IGenreRepository
    {
        /// <summary>
        /// Get top genre of a customer.
        /// </summary>
        /// <param name="id">Used to get specific customer.</param>
        /// <returns>Top genre of a customer.</returns>
        public IEnumerable<CustomerGenre> GetAll(int id);
    }
}
