﻿using SQLAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssignment.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        /// <summary>
        /// Gets a specific customer by first name. Doesnt have to be full name. 
        /// </summary>
        /// <param name="firstName">First name of the customers that you want to get.</param>
        /// <returns>Returns a specific customer by first name</returns>
        public Customer GetCustomerByName(string firstName);

        /// <summary>
        /// Gets customers from the database with offset and limit.
        /// </summary>
        /// <param name="offset">Sets how many customers you want to get.</param>
        /// <param name="limit">Sets where you want the offset to start.</param>
        /// <returns>Returns a group of customers depending on offset and limit.</returns>
        public List<Customer> GetCustomersByLimit(int offset, int limit);

    }
}
