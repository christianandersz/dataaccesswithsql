﻿using Microsoft.Data.SqlClient;
using SQLAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssignment.Repositories
{
    public class SpenderRepository : ISpenderRepository
    {

        /// <summary>
        /// Gets the total spending of each customer and orders by the top spender.
        /// </summary>
        /// <returns>Returns total spending of each customer and orders by the top spender.</returns>
        IEnumerable<CustomerSpender> ISpenderRepository.GetAll()
        {
            List<CustomerSpender> spenderList = new List<CustomerSpender>();
            string sql = "SELECT DISTINCT CustomerId, SUM(Total) AS Total FROM Invoice GROUP BY CustomerId ORDER BY SUM(Total) DESC";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle result
                            while (reader.Read())
                            {
                                CustomerSpender spender = new CustomerSpender();
                                spender.CustomerId = reader.GetInt32(0);
                                spender.Total = reader.GetDecimal(1);

                                spenderList.Add(spender);
                            }
                        }
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return spenderList;
        }
    }
}
