﻿using Microsoft.Data.SqlClient;
using SQLAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssignment.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        /// <summary>
        /// Get all customers from the database.
        /// </summary>
        /// <returns>Returns all the customers from the database.</returns>
        IEnumerable<Customer> IRepository<Customer>.GetAll()
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer";

            try
            {
                // Connect 
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle result
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader["CustomerId"] != DBNull.Value ? reader.GetInt32(0) : 0;
                                customer.FirstName = reader["FirstName"] != DBNull.Value ? reader.GetString(1) : "Null";
                                customer.LastName = reader["LastName"] != DBNull.Value ? reader.GetString(2) : "Null";
                                customer.Country = reader["Country"] != DBNull.Value ? reader.GetString(3) : "Null";
                                customer.PostalCode = reader["PostalCode"] != DBNull.Value ? reader.GetString(4) : "Null";
                                customer.Phone = reader["Phone"] != DBNull.Value ? reader.GetString(5) : "Null";
                                customer.Email = reader["Email"] != DBNull.Value ? reader.GetString(6) : "Null";

                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return customerList;

        }

        /// <summary>
        /// Gets a specific customer by its id.
        /// </summary>
        /// <param name="id">Id of the customer that you want to get.</param>
        /// <returns>Returns a specific customer by id.</returns>
        public Customer GetById(int id)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = @CustomerId";

            try
            {
                // Connect 
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader["FirstName"] != DBNull.Value ? reader.GetString(1) : "Null";
                                customer.LastName = reader["LastName"] != DBNull.Value ? reader.GetString(2) : "Null";
                                customer.Country = reader["Country"] != DBNull.Value ? reader.GetString(3) : "Null";
                                customer.PostalCode = reader["PostalCode"] != DBNull.Value ? reader.GetString(4) : "Null";
                                customer.Phone = reader["Phone"] != DBNull.Value ? reader.GetString(5) : "Null";
                                customer.Email = reader["Email"] != DBNull.Value ? reader.GetString(6) : "Null";
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }
            return customer;
        }

        /// <summary>
        /// Gets a specific customer by first name. Doesnt have to be full name. 
        /// </summary>
        /// <param name="firstName">First name of the customers that you want to get.</param>
        /// <returns>Returns a specific customer by first name</returns>
        public Customer GetCustomerByName(string firstName)
        {
            Customer customer = new Customer();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE FirstName LIKE '%" + firstName + "%'";

            try
            {
                // Connect 
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", firstName);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                // Handle result
                                customer.CustomerId = reader.GetInt32(0);
                                customer.FirstName = reader["FirstName"] != DBNull.Value ? reader.GetString(1) : "Null";
                                customer.LastName = reader["LastName"] != DBNull.Value ? reader.GetString(2) : "Null";
                                customer.Country = reader["Country"] != DBNull.Value ? reader.GetString(3) : "Null";
                                customer.PostalCode = reader["PostalCode"] != DBNull.Value ? reader.GetString(4) : "Null";
                                customer.Phone = reader["Phone"] != DBNull.Value ? reader.GetString(5) : "Null";
                                customer.Email = reader["Email"] != DBNull.Value ? reader.GetString(6) : "Null";
                            }
                        }
                    }
                }
            }
            catch (SqlException e)
            {

                Console.WriteLine(e);
            }
            return customer;
        }

        /// <summary>
        /// Adds a new customer to the database. 
        /// </summary>
        /// <param name="customer">Is the new customer that you want to add to the database.</param>
        /// <returns>Ádds it to the database.</returns>
        public bool Add(Customer customer)
        {
            bool success = false;
            string sql = "INSERT INTO Customer(FirstName, LastName, Country, PostalCode, Phone, Email) " +
                "VALUES (@FirstName, @LastName, @Country, @PostalCode, @Phone, @Email)";

            try
            {
                // Connect 
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
            }

            return success;
        }

        /// <summary>
        /// Edits a customer from the database.
        /// </summary>
        /// <param name="customer">Is the customer that you want to edit from the database.</param>
        /// <returns>Edits a customer in the database.</returns>
        public bool Edit(Customer customer)
        {
            bool success = false;
            string sql = "UPDATE Customer SET FirstName = @FirstName, LastName = @LastName, Country = @Country, PostalCode = @PostalCode, Phone = @Phone, Email = @Email WHERE CustomerId = @CustomerId";

            try
            {
                // Connect 
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@FirstName", customer.FirstName);
                        cmd.Parameters.AddWithValue("@LastName", customer.LastName);
                        cmd.Parameters.AddWithValue("@Country", customer.Country);
                        cmd.Parameters.AddWithValue("@PostalCode", customer.PostalCode);
                        cmd.Parameters.AddWithValue("@Phone", customer.Phone);
                        cmd.Parameters.AddWithValue("@Email", customer.Email);
                        cmd.Parameters.AddWithValue("@CustomerId", customer.CustomerId);
                        success = cmd.ExecuteNonQuery() > 0 ? true : false;
                    }
                }
            }
            catch (Exception e)
            {

                Console.WriteLine(e);
            }

            return success;
        }

        /// <summary>
        /// Gets customers from the database with offset and limit.
        /// </summary>
        /// <param name="offset">Sets how many customers you want to get.</param>
        /// <param name="limit">Sets where you want the offset to start.</param>
        /// <returns>Returns a group of customers depending on offset and limit.</returns>
        public List<Customer> GetCustomersByLimit(int offset, int limit)
        {
            List<Customer> customerList = new List<Customer>();
            string sql = "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer ORDER BY CustomerId OFFSET @OFFSET ROWS FETCH NEXT @LIMIT ROWS ONLY";

            try
            {
                // Connect 
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@OFFSET", offset);
                        cmd.Parameters.AddWithValue("@LIMIT", limit);

                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle result
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerId = reader["CustomerId"] != DBNull.Value ? reader.GetInt32(0) : 0;
                                customer.FirstName = reader["FirstName"] != DBNull.Value ? reader.GetString(1) : "Null";
                                customer.LastName = reader["LastName"] != DBNull.Value ? reader.GetString(2) : "Null";
                                customer.Country = reader["Country"] != DBNull.Value ? reader.GetString(3) : "Null";
                                customer.PostalCode = reader["PostalCode"] != DBNull.Value ? reader.GetString(4) : "Null";
                                customer.Phone = reader["Phone"] != DBNull.Value ? reader.GetString(5) : "Null";
                                customer.Email = reader["Email"] != DBNull.Value ? reader.GetString(6) : "Null";

                                customerList.Add(customer);
                            }
                        }
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return customerList;
        }
    }
}
