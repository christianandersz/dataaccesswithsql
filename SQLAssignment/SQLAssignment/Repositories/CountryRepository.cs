﻿using Microsoft.Data.SqlClient;
using SQLAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssignment.Repositories
{
    public class CountryRepository : ICountryRepository
    {
        /// <summary>
        /// Get quantity of customers in each country. Ordered by most customers.  
        /// </summary>
        /// <returns>Returns quantity of customers in each country.</returns>
        public IEnumerable<CustomerCountry> GetAll()
        {
            List<CustomerCountry> countryList = new List<CustomerCountry>();
            string sql = "SELECT Customer.Country, COUNT(CustomerId) AS Customers FROM Customer GROUP BY Country ORDER BY Customers DESC";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle result
                            while (reader.Read())
                            {
                                CustomerCountry customerCountry = new CustomerCountry();
                                customerCountry.Country = reader.GetString(0);
                                customerCountry.CustomerId = reader.GetInt32(1);


                                countryList.Add(customerCountry);
                            }
                        }
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return countryList;
        }

    }
}
