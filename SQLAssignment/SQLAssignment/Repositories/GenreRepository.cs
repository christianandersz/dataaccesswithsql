﻿using Microsoft.Data.SqlClient;
using SQLAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssignment.Repositories
{
    public class GenreRepository : IGenreRepository
    {
        /// <summary>
        /// Get top genre of a customer.
        /// </summary>
        /// <param name="id">Used to get specific customer.</param>
        /// <returns>Top genre of a customer.</returns>
        public IEnumerable<CustomerGenre> GetAll(int id)
        {
            List<CustomerGenre> genreList = new List<CustomerGenre>();
            string sql = "SELECT TOP 1 WITH TIES c.FirstName as Customer_Name, g.Name as Genre_Name FROM((((Customer c INNER JOIN Invoice i ON c.CustomerId = i.CustomerId) INNER JOIN InvoiceLine il ON i.InvoiceId = il.InvoiceId) INNER JOIN Track t ON il.TrackId = t.TrackId) INNER JOIN Genre g ON t.GenreId = g.GenreId) WHERE c.CustomerId = @CustomerId GROUP BY c.FirstName, g.Name ORDER BY COUNT(il.TrackId) desc";

            try
            {
                using (SqlConnection connection = new SqlConnection(ConnectionStringHelper.GetConnectionString()))
                {
                    connection.Open();
                    // Make a command
                    using (SqlCommand cmd = new SqlCommand(sql, connection))
                    {
                        cmd.Parameters.AddWithValue("@CustomerId", id);
                        // Reader
                        using (SqlDataReader reader = cmd.ExecuteReader())
                        {
                            // Handle result
                            while (reader.Read())
                            {
                                CustomerGenre genre = new CustomerGenre();
                                genre.FirstName = reader.GetString(0);
                                genre.Genre = reader.GetString(1);

                                genreList.Add(genre);
                            }
                        }
                    }
                }
            }

            catch (SqlException e)
            {
                Console.WriteLine(e);
            }
            return genreList;
        }
    }
}
