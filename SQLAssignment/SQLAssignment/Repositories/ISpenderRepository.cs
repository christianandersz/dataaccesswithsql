﻿using SQLAssignment.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssignment.Repositories
{
    public interface ISpenderRepository
    {
        /// <summary>
        /// Gets the total spending of each customer and orders by the top spender.
        /// </summary>
        /// <returns>Returns total spending of each customer and orders by the top spender.</returns>
        public IEnumerable<CustomerSpender> GetAll();
    }
}
