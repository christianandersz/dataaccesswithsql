﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SQLAssignment.Repositories
{
    public interface IRepository<T>
    {
        /// <summary>
        /// Extended method to get an object by id.
        /// </summary>
        /// <param name="id">Id of the object you want to get.</param>
        /// <returns>A object by id.</returns>
        T GetById(int id);

        /// <summary>
        /// Extended method to get all objects in database.
        /// </summary>
        /// <returns>All objects in specific table.</returns>
        IEnumerable<T> GetAll();

        /// <summary>
        /// Adds an object to specific table in database.
        /// </summary>
        /// <param name="entity">Genereic name</param>
        /// <returns>Adds an object to specific table in database.</returns>
        bool Add(T entity);

        /// <summary>
        /// Edits and object from specific table in database.
        /// </summary>
        /// <param name="entity">Generic name</param>
        /// <returns>Edits an object in specific table in database.</returns>
        bool Edit(T entity);

        
    }
}
