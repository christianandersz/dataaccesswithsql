USE SuperheroDB;

CREATE TABLE Superhero(
	superhero_Id int NOT NULL IDENTITY(1,1) PRIMARY KEY (superhero_Id),
	full_name nvarchar(50) NULL,
	alias nvarchar(30) NULL, 
	origin nvarchar(60) NULL,
);

CREATE TABLE Assistant(
	assistant_Id int NOT NULL IDENTITY(1,1) PRIMARY KEY (assistant_Id),
	full_name nvarchar(50) NULL,
);

CREATE TABLE SuperheroPower(
	power_Id int NOT NULL IDENTITY(1,1) PRIMARY KEY (power_Id),
	power_name nvarchar(50) NULL,
	power_description nvarchar(255) NULL
);