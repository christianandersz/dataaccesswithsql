USE SuperheroDB

CREATE TABLE Superheropowerrelation(
	superhero_Id int NOT NULL FOREIGN KEY REFERENCES Superhero(superhero_Id),
	power_Id int NOT NULL FOREIGN KEY REFERENCES SuperheroPower(power_Id),
	PRIMARY KEY (superhero_Id, power_Id)
);