USE [SuperheroDB]
GO

INSERT INTO Superhero
           (full_name
           ,alias
           ,origin)
     VALUES
           ( 'Iron Man',
			'Tony Stark',
            'Long Island')
GO

INSERT INTO Superhero
           (full_name
           ,alias
           ,origin)
     VALUES
           ( 'Batman',
			'Bruce Wayne',
            'Gotham City')

GO

INSERT INTO Superhero
           (full_name
           ,alias
           ,origin)
     VALUES
           ( 'Super Man',
			'Clark Kent',
            'Krypton')

GO




