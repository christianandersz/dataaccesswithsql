USE [SuperheroDB]
GO

INSERT INTO SuperheroPower
           (power_name
           ,power_description)
     VALUES
           ('Fire Breath',
           'The ability to generate from within oneself fire and release them from the mouth.'),
		   ('Camouflage',
		   'The power to visually blend into immediate environment. Not to be confused with Invisibility.'),
		   ('Flying', 
		   'The power to fly without any outside influence.'),
		   ('Enhanced Speed',
		   'The power to possess speed beyond that of the peak members of their species. Sub-power of Enhanced Body.')
GO

INSERT INTO superheropowerrelation (superhero_Id , power_Id) 
	VALUES 
		(1,1), 
		(1,3),
		(2,3),
		(2,7),
		(3,2);

GO

		
